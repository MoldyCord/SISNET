﻿using Newtonsoft.Json;
using Sisnet.Models.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Clientes
{
    [Table("dbo.DireccionesEnvio")]
    public class DireccionEnvio : Direccion
    {

        [Display(Name = "¿Seleccionar ésta dirección como predeterminada?")]
        public Boolean isDefault { get; set; }

        [JsonIgnore]
        public virtual Cliente Cliente { get; set; }
    }

}