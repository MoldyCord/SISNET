﻿using Newtonsoft.Json;
using Sisnet.Models.Carrito;
using Sisnet.Models.Clientes;
using Sisnet.Models.Inventario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Personas
{

    [Table("dbo.Clientes")]
    public class Cliente : Persona
    {

        public Cliente() { }


        [JsonIgnore]
        public virtual ICollection<DireccionEnvio> DireccionesEnvio { get; set; }

       [JsonIgnore]
        public virtual ICollection<Card> Cards { get; set; }

        [JsonIgnore]
        public virtual ICollection<OrdenServicio> HistorialServicios { get; set; }

        [JsonIgnore]
        public virtual ICollection<Pedido> HistorialPedidos { get; set; }

        [Display(Name = "Miembro desde:")]
        [DataType(DataType.DateTime)]
        public DateTime FechaAfiliacion { get; set; }



    }
}