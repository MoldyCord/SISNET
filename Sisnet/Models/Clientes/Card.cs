﻿using Newtonsoft.Json;
using Sisnet.Models.Personas;
using System;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Sisnet.Models.Clientes
{

    [Table("dbo.Cards")]
    public class Card
    {
        [Key]
        public String idTarjeta { get; set; }

        [Display(Name = "Nombre en la tarjeta")]
        public string nombreTarjeta { get; set; }

        [Required]
        [RegularExpression("^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35d{3})d{11})$", ErrorMessage = "Ingrese un número válido")]
        [Display(Name = "Número de tarjeta")]
        public string numTarjeta { get; set; }

        [Required]
        [Display(Name = "CSV")]
        [RegularExpression("^[0-9]{3}$", ErrorMessage = "El codigo de seguridad es de 3 digitos")]
        public string codigoSeguridad { get; set; }

        public virtual Cliente Cliente { get; set; }
      

    }
}