﻿
using Sisnet.Models.Clientes;
using Sisnet.Models.Inventario;
using Sisnet.Models.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Carrito
{
    [Table("dbo.Pedidos")]
    public class Pedido
    {
        [Key]
        public String IdPedido { get; set; }

        [Display(Name = "Folio")]
        public String FolioPedido { get; set; }

        [Display(Name = "Total")]
        public Double Total { get; set; }

        public StatusPedido Estado { get; set; }

        public ICollection<ItemPedido> ItemsPedido { get; set; }

        //public virtual DireccionEnvio Direccion { get; set; }

        //public virtual Card Card { get; set; }

        public enum StatusPedido
        {
            SOLICITADO = 1,
            DESPACHADO = 2,
            ENVIADO = 3,
            ENTREGADO = 4,
            NO_ENTREGADO = 5,
            CANCELADO = 6
        }
    }
}