﻿using Sisnet.Models.Inventario;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Carrito
{

    [Table("dbo.ItemsPedido")]
    public class ItemPedido
    {
        [Key]
        public string id { get; set; }
        [ForeignKey("Pedido")]
        public string idPedido { get; set; }
        public Pedido Pedido { get; set; }
        [ForeignKey("Producto")]
        public string idProducto { get; set; }
        public Producto Producto { get; set; }
        public int Cantidad { get; set; }
    
        


    }
}