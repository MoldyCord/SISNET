﻿using Sisnet.Models.Inventario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Carrito
{
    public class ItemCarrito : IEquatable<ItemCarrito>
    {
        private Producto _producto;
        private int _cantidad;
        public Producto Producto { get => _producto; set => _producto = value; }
        public int Cantidad { get => _cantidad; set => _cantidad = value; }

        public ItemCarrito() { }

        public ItemCarrito(Producto producto, int cantidad) {
            this._producto = producto;
            this._cantidad = cantidad;

        }

        public bool Equals(ItemCarrito other)
        {
            throw new NotImplementedException();
        }
    }
}