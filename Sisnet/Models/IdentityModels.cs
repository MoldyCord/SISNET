﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Sisnet.Models.Carrito;
using Sisnet.Models.Clientes;
using Sisnet.Models.Inventario;
using Sisnet.Models.Personas;

namespace Sisnet.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public string UserRole { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {


        public virtual DbSet<Persona> personas { get; set; }
        public virtual DbSet<Direccion> direcciones { get; set; }
        public virtual DbSet<Cliente> clientes { get; set; }
        public virtual DbSet<Card> TarjetasPago { get; set; }
        public virtual DbSet<DireccionEnvio> direccionesEnvio { get; set; }
        public virtual DbSet<FilePath> FilePaths { get; set; }


        public virtual DbSet<Producto> productos { get; set; }
        public virtual DbSet<ProductoMonitor> productosMonitor { get; set; }
        public virtual DbSet<ProductoProcesador> productosProcesador { get; set; }
        public virtual DbSet<ProductoHardDrive> productosHardDrives { get; set; }

        public virtual DbSet<Pedido> Pedidos { get; set; }
      
        public virtual DbSet<OrdenServicio> OrdenesServicio { get; set; }
        public virtual DbSet<ItemPedido> ItemsPedido { get; set; }
        public virtual DbSet<ProductoGabinete> ProductoGabinetes { get; set; }

        public virtual DbSet<ProductoMemoria> ProductoMemorias { get; set; }

        public virtual DbSet<ProductoPeriferico> ProductoPerifericos { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

       
    }
}