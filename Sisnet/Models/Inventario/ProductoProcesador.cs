﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosProcesador")]
    public class ProductoProcesador : Producto
    {

        [Display(Name = "Número de núcleos")]
        public string Nucleos { get; set; }

        [Display(Name = "Número de hilos")]
        public string Hilos { get; set; }

        [Display(Name = "Velocidad(En Ghz)")]
        public string VelocidadProcesador { get; set; }

        [Display(Name = "Cáche")]
        public string CacheProcesador { get; set; }


    }
}