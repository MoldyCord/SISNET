﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

        
namespace Sisnet.Models.Inventario
{
    [Table("dbo.Productos")]
    public abstract class Producto
    {
        [Key]
        public string IdProducto { get; set; }

        [Required]
        [Display(Name = "Nombre del producto")]
        public string NombreProducto { get; set; }

        [Required(ErrorMessage ="El precio debe ser un número")]
        [Display(Name = "Precio")]
        public Double PrecioProducto { get; set; }

        [Display(Name = "SKU")]
        public string SKU { get; set; }

        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name = "Marca")]
        public string marca { get; set; }

        [Display(Name = "Modelo")]
        public string Modelo { get; set; } 

        [Display(Name ="Número de serie")]
        public string numSerie { get; set; }

        [Required]
        [Display(Name="Existencia")]
        public int Existencia { get; set; }

        public virtual FilePath ImagenProducto { get; set; }

    }
}