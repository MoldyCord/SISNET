﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosDiscos")]
    public class ProductoHardDrive : Producto
    {

        [Display(Name = "Capacidad")]
        public int Capacidad { get; set; }

        [Display(Name = "Velocidad de Lectura/Escritura")]
        public String Velocidad { get; set; }

        [Display(Name = "Puerto")]
        public String Puerto { get; set; }

        [Display(Name = "Tipo")]
        public Tipo Type { get; set; }

        public enum Tipo
        {
            SSD = 1,
            HDD = 2
        }
    }
}