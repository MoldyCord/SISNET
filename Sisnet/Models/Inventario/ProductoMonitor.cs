﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosMonitor")]
    public class ProductoMonitor : Producto
    {

        [Display(Name = "Tamaño")]
        public string Tamanio { get; set; }

        [Display(Name = "Salidas de video")]
        public string SalidasVideo { get; set; }

        [Display(Name = "Ratio de actualización")]
        public string RatioDeRefresh { get; set; }

        [Display(Name = "Resolución")]
        public string Resolution { get; set; }

        [Display(Name = "Color")]
        public string Color { get; set; }


    }
}