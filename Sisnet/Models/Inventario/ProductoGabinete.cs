﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosGabinete")]
    public class ProductoGabinete : Producto
    {   
        [Display(Name ="Dimensiones")]
        public String Dimensiones { get; set; }

        [Display(Name = "Color")]
        public String Color { get; set; }

        [Display(Name = "Puertos del panel frontal")]
        public String Puertos { get; set; }
        
        [Display(Name = "Ventiladores")]
        public String Ventiladores { get; set; }
    }
}