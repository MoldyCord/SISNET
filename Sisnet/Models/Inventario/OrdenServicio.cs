﻿using Sisnet.Models.Personas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.OrdenesServicio")]
    public class OrdenServicio
    {

        [Key]
        public String IdOrden { get; set; }

        public String IdCliente { get; set; }

        [Display(Name = "Folio")]
        public String Folio { get; set; }

        public virtual Cliente Cliente { get; set; }

        [Display(Name = "Fecha de Recibo")]
        public DateTime FechaRecibo { get; set; }

        [Display(Name = "Fecha estimada de Entrega")]
        public DateTime FechaEntrega { get; set; }

        [Display(Name = "Estado")]
        public Status Estado { get; set; }

        [Display(Name = "Descripción")]
        public String Descripcion { get; set; }

        [Display(Name = "Costo")]
        public Double Costo { get; set; }

        [Display(Name = "Observaciones")]
        public String Observaciones { get; set; }

        public enum Status {

            EN_PROCESO=1,
            EN_ESPERA=2,
            EN_PAUSA=3,
            FINALIZADO=4,
            ENTREGADO=5
        }
    }
}