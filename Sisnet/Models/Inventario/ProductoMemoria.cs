﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosMemorias")]
    public class ProductoMemoria : Producto
    {

        [Display(Name = "Capacidad")]
        public String Capacidad { get; set; }

        [Display(Name = "Velocidad")]
        public String Velocidad { get; set; }

        [Display(Name = "Clase")]
        public String Clase { get; set; }

        [Display(Name ="Latencia")]
        public String Latencia { get; set; }
    }
}