﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Inventario
{
    [Table("dbo.ProductosPeriferico")]
    public class ProductoPeriferico : Producto
    {
        [Display(Name = "Color")]
        public String Color { get; set; }

        [Display(Name = "Tipo")]
        public String Tipo { get; set; }

        [Display(Name = "Compatibilidad")]
        public String Compatibilidad { get; set; }

        [Display(Name = "Interfaz")]
        public String Interfaz { get; set; }
    }
}