﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Personas
{
    [Table("dbo.Personas")]
    public abstract class Persona
    {
        [Key]
        public string idPersona { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "Nombre(s)")]
        public string nombre { get; set; }

        [Display(Name = "Apellido Paterno")]
        public string apellidoPaterno { get; set; }

        [Display(Name = "Apellido Materno")]
        public string apellidoMaterno { get; set; }

        [Display(Name = "CURP")]
        [RegularExpression("[A-Za-z]{4}[0-9]{6}[HMhm]{1}[A-Za-z]{5}[A-Za-z0-9]{1}[0-9]{1}", ErrorMessage = "CURP incorrecto, verifique el formato")]
        public string CURP { get; set; }

        [Display(Name = "RFC")]
        public string RFC { get; set; }

        [Display(Name = "Nacionalidad")]
        public string Nacionalidad { get; set; }


        [NotMapped]
        public string fullName
        {
            get
            {
                return (this.apellidoPaterno + " " + this.apellidoMaterno + " " + this.nombre).ToUpper();
            }
        }
        [NotMapped]
        public string NameUser
        {
            get
            {
                return (this.nombre + " " + this.apellidoPaterno);
            }
        }
        


       
        public enum Sexo
        {
            MASCULINO, FEMENINO
        }

        public enum EstadoCivil
        {
            [Display(Name = "Soltero(a)")]
            SOLTERO,
            [Display(Name = "Casado(a)")]
            CASADO,
            [Display(Name = "Divorciado(a)")]
            DIVORCIADO,
            [Display(Name = "Viudo(a)")]
            VIUDO,
            [Display(Name = "Unión Libre")]
            UNION_LIBRE,
            [Display(Name = "Otro")]
            OTRO
        }


        public enum TipoSangre
        {
            [Display(Name = "O-")]
            O_NEGATIVO,
            [Display(Name = "O+")]
            O_POSITIVO,
            [Display(Name = "A-")]
            A_NEGATIVO,
            [Display(Name = "A+")]
            A_POSITIVO,
            [Display(Name = "B-")]
            B_NEGATIVO,
            [Display(Name = "B+")]
            B_POSITIVO,
            [Display(Name = "AB-")]
            AB_NEGATIVO,
            [Display(Name = "AB+")]
            AB_POSITIVO,
            [Display(Name = "Otro")]
            OTRO
        }



    }
}