﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Sisnet.Models.Personas
{
    [Table("dbo.Direcciones")]
    public abstract class Direccion
    {

        [Key]
        public String IdDireccion { get; set; }
        [Display(Name = "Calle")]
        [MaxLength(100)]
        public string calle { get; set; }
        [Display(Name = "Manzana")]
        [MaxLength(100)]
        public string manzana { get; set; }
        [Display(Name = "Lote")]
        [MaxLength(100)]
        public string lote { get; set; }
        [Display(Name = "Número Interior")]
        [MaxLength(100)]
        public string noInterior { get; set; }
        [Display(Name = "Número Exterior")]
        [MaxLength(100)]
        public string noExterior { get; set; }
        [Display(Name = "Colonia")]
        [MaxLength(100)]
        public string colonia { get; set; }
        [Display(Name = "Municipio")]
        [MaxLength(100)]
        public string municipio { get; set; }
        [Required]
        [DataType(DataType.PostalCode)]
        [MaxLength(10)]
        [Display(Name = "Código Postal")]
        public string cp { get; set; }
        [MaxLength(100)]
        [Display(Name = "Ciudad")]
        public string ciudad { get; set; }
        [MaxLength(100)]
        [Display(Name = "Estado")]
        public string estado { get; set; }
        [MaxLength(100)]
        [Display(Name = "País")]
        public string pais { get; set; }
        

        [NotMapped]
        public string fullDireccion
        {
            get
            {
                return (this.calle + "," + this.manzana + "," + this.lote +","+ this.colonia+","+this.municipio).ToUpper();
            }
        }

    }

}