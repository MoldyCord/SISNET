﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Inventario
{
    public class ProductoHardDriveController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductoHardDrive
        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View(db.productosHardDrives.ToList());
        }

        // GET: ProductoHardDrive/Details/5
        [Authorize(Roles ="Admin")]
        public ActionResult Details(string id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoHardDrive productoHardDrive = db.productosHardDrives.Include(i => i.ImagenProducto).SingleOrDefault(i => i.IdProducto == id);
            
            if (productoHardDrive == null)
            {
                return HttpNotFound();
            }
            return View(productoHardDrive);
        }

        // GET: ProductoHardDrive/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoHardDrive/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia,Capacidad,Velocidad,Puerto,Type")] ProductoHardDrive productoHardDrive, HttpPostedFileBase upload)
        {

            if (ModelState.IsValid)
            {
                productoHardDrive.IdProducto = Guid.NewGuid().ToString();

                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoHardDrive.ImagenProducto = photo;
                
                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.productosHardDrives.Add(productoHardDrive);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoHardDrive);
        }

        // GET: ProductoHardDrive/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoHardDrive productoHardDrive = db.productosHardDrives.Include(i => i.ImagenProducto).SingleOrDefault(i => i.IdProducto == id);
            if (productoHardDrive == null)
            {
                return HttpNotFound();
            }
            return View(productoHardDrive);
        }

        // POST: ProductoHardDrive/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia,Capacidad,Velocidad,Puerto,Type")] ProductoHardDrive productoHardDrive)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoHardDrive).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoHardDrive);
        }

        // GET: ProductoHardDrive/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoHardDrive productoHardDrive = db.productosHardDrives.Include(i => i.ImagenProducto).SingleOrDefault(i => i.IdProducto == id);
            if (productoHardDrive == null)
            {
                return HttpNotFound();
            }
            return View(productoHardDrive);
        }

        // POST: ProductoHardDrive/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductoHardDrive productoHardDrive = db.productosHardDrives.Include(i => i.ImagenProducto).SingleOrDefault(i => i.IdProducto == id);
            db.productosHardDrives.Remove(productoHardDrive);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        public ActionResult ShopDisco()
        { 

            return View(db.productosHardDrives.Include(i=>i.ImagenProducto).ToList());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
