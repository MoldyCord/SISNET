﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Inventario
{
    public class ProductoMemoriasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductoMemorias
        public ActionResult Index()
        {
            return View(db.ProductoMemorias.ToList());
        }

        // GET: ProductoMemorias/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoMemoria productoMemoria = db.ProductoMemorias.Find(id);
            if (productoMemoria == null)
            {
                return HttpNotFound();
            }
            return View(productoMemoria);
        }

        // GET: ProductoMemorias/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoMemorias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,Capacidad,Velocidad,Clase,Latencia,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoMemoria productoMemoria,HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                productoMemoria.IdProducto = Guid.NewGuid().ToString();
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoMemoria.ImagenProducto = photo;

                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.ProductoMemorias.Add(productoMemoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoMemoria);
        }

        // GET: ProductoMemorias/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoMemoria productoMemoria = db.ProductoMemorias.Find(id);
            if (productoMemoria == null)
            {
                return HttpNotFound();
            }
            return View(productoMemoria);
        }

        // POST: ProductoMemorias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,Capacidad,Velocidad,Clase,Latencia,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoMemoria productoMemoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoMemoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoMemoria);
        }

        // GET: ProductoMemorias/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoMemoria productoMemoria = db.ProductoMemorias.Find(id);
            if (productoMemoria == null)
            {
                return HttpNotFound();
            }
            return View(productoMemoria);
        }

        // POST: ProductoMemorias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductoMemoria productoMemoria = db.ProductoMemorias.Find(id);
            db.ProductoMemorias.Remove(productoMemoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ShopMemoria()
        {

            return View(db.ProductoMemorias.Include(i => i.ImagenProducto).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
