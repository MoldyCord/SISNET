﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Inventario
{
    public class ProductoPerifericoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductoPerifericoes
        public ActionResult Index()
        {
            return View(db.ProductoPerifericos.ToList());
        }

        // GET: ProductoPerifericoes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoPeriferico productoPeriferico = db.ProductoPerifericos.Find(id);
            if (productoPeriferico == null)
            {
                return HttpNotFound();
            }
            return View(productoPeriferico);
        }

        // GET: ProductoPerifericoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoPerifericoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,Color,Tipo,Compatibilidad,Interfaz,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoPeriferico productoPeriferico,HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {

                productoPeriferico.IdProducto = Guid.NewGuid().ToString();
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoPeriferico.ImagenProducto = photo;

                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.ProductoPerifericos.Add(productoPeriferico);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoPeriferico);
        }

        // GET: ProductoPerifericoes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoPeriferico productoPeriferico = db.ProductoPerifericos.Find(id);
            if (productoPeriferico == null)
            {
                return HttpNotFound();
            }
            return View(productoPeriferico);
        }

        // POST: ProductoPerifericoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,Color,Tipo,Compatibilidad,Interfaz,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoPeriferico productoPeriferico)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoPeriferico).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoPeriferico);
        }

        // GET: ProductoPerifericoes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoPeriferico productoPeriferico = db.ProductoPerifericos.Find(id);
            if (productoPeriferico == null)
            {
                return HttpNotFound();
            }
            return View(productoPeriferico);
        }

        // POST: ProductoPerifericoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductoPeriferico productoPeriferico = db.ProductoPerifericos.Find(id);
            db.ProductoPerifericos.Remove(productoPeriferico);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ShopPeriferico()
        {

            return View(db.ProductoPerifericos.Include(i => i.ImagenProducto).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
