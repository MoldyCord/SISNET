﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Clientes
{
    public class ProductoMonitorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductoMonitors
        public ActionResult Index()
        {
            return View(db.productosMonitor.ToList());
        }

        // GET: ProductoMonitors/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productoMonitor = db.productosMonitor.Find(id);
            if (productoMonitor == null)
            {
                return HttpNotFound();
            }
            return View(productoMonitor);
        }

        // GET: ProductoMonitors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoMonitors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Tamanio,SalidasVideo,RatioDeRefresh,Resolution,Color")] ProductoMonitor productoMonitor, HttpPostedFileBase upload)
        {

           
            if (ModelState.IsValid)
            {
                productoMonitor.IdProducto = Guid.NewGuid().ToString();
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoMonitor.ImagenProducto = photo;

                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.productosMonitor.Add(productoMonitor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoMonitor);
        }

        // GET: ProductoMonitors/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productoMonitor = db.productosMonitor.Find(id);
            if (productoMonitor == null)
            {
                return HttpNotFound();
            }
            return View(productoMonitor);
        }

        // POST: ProductoMonitors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Tamanio,SalidasVideo,RatioDeRefresh,Resolution,Color")] ProductoMonitor productoMonitor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoMonitor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoMonitor);
        }

        // GET: ProductoMonitors/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productoMonitor = db.productosMonitor.Find(id);
            if (productoMonitor == null)
            {
                return HttpNotFound();
            }
            return View(productoMonitor);
        }

        // POST: ProductoMonitors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            var productoMonitor = db.productosMonitor.Find(id);
            db.productos.Remove(productoMonitor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ShopMonitor()
        {

            return View(db.productosMonitor.Include(i => i.ImagenProducto).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
