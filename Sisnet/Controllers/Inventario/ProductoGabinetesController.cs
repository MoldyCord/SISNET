﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Inventario
{
    public class ProductoGabinetesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductoGabinetes
        public ActionResult Index()
        {
            return View(db.ProductoGabinetes.ToList());
        }

        // GET: ProductoGabinetes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoGabinete productoGabinete = db.ProductoGabinetes.Find(id);
            if (productoGabinete == null)
            {
                return HttpNotFound();
            }
            return View(productoGabinete);
        }

        // GET: ProductoGabinetes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoGabinetes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,Dimensiones,Color,Puertos,Ventiladores,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoGabinete productoGabinete,HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                productoGabinete.IdProducto = Guid.NewGuid().ToString();
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoGabinete.ImagenProducto = photo;

                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.ProductoGabinetes.Add(productoGabinete);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoGabinete);
        }

        // GET: ProductoGabinetes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoGabinete productoGabinete = db.ProductoGabinetes.Find(id);
            if (productoGabinete == null)
            {
                return HttpNotFound();
            }
            return View(productoGabinete);
        }

        // POST: ProductoGabinetes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,Dimensiones,Color,Puertos,Ventiladores,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie,Existencia")] ProductoGabinete productoGabinete)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoGabinete).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoGabinete);
        }

        // GET: ProductoGabinetes/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoGabinete productoGabinete = db.ProductoGabinetes.Find(id);
            if (productoGabinete == null)
            {
                return HttpNotFound();
            }
            return View(productoGabinete);
        }

        // POST: ProductoGabinetes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductoGabinete productoGabinete = db.ProductoGabinetes.Find(id);
            db.ProductoGabinetes.Remove(productoGabinete);
            db.SaveChanges();
            return RedirectToAction("Index");

        }

        public ActionResult ShopGabinete()
        {

            return View(db.ProductoGabinetes.Include(i => i.ImagenProducto).ToList());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
