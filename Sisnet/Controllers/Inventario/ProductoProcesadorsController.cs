﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sisnet.Models;
using Sisnet.Models.Inventario;

namespace Sisnet.Controllers.Inventario
{
    public class ProductoProcesadorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize(Roles = "Admin")]
        // GET: ProductoProcesadors
        public ActionResult Index()
        {
            return View(db.productosProcesador.ToList());
        }

        // GET: ProductoProcesadors/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoProcesador productoProcesador = db.productosProcesador.Find(id);
            if (productoProcesador == null)
            {
                return HttpNotFound();
            }
            return View(productoProcesador);
        }

        // GET: ProductoProcesadors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProductoProcesadors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdProducto,Nucleos,Hilos,VelocidadProcesador,CacheProcesador,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie")] ProductoProcesador productoProcesador,HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                productoProcesador.IdProducto = Guid.NewGuid().ToString();
                if (upload != null && upload.ContentLength > 0)
                {
                    var photo = new FilePath
                    {
                        FileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(upload.FileName),
                        FileType = FileType.Photo
                    };
                    productoProcesador.ImagenProducto = photo;

                    var fileSavePath = (HttpContext.Server.MapPath("~/images/") + photo.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    upload.SaveAs(fileSavePath);
                }
                db.productosProcesador.Add(productoProcesador);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productoProcesador);
        }

        // GET: ProductoProcesadors/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoProcesador productoProcesador = db.productosProcesador.Find(id);
            if (productoProcesador == null)
            {
                return HttpNotFound();
            }
            return View(productoProcesador);
        }

        // POST: ProductoProcesadors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdProducto,Nucleos,Hilos,VelocidadProcesador,CacheProcesador,NombreProducto,PrecioProducto,SKU,Descripcion,marca,Modelo,numSerie")] ProductoProcesador productoProcesador)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productoProcesador).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productoProcesador);
        }

        // GET: ProductoProcesadors/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductoProcesador productoProcesador = db.productosProcesador.Find(id);
            if (productoProcesador == null)
            {
                return HttpNotFound();
            }
            return View(productoProcesador);
        }

        // POST: ProductoProcesadors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ProductoProcesador productoProcesador = db.productosProcesador.Find(id);
            db.productosProcesador.Remove(productoProcesador);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult ShopProcesador()
        {

            return View(db.productosProcesador.Include(i => i.ImagenProducto).ToList());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
