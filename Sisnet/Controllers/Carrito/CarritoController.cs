﻿using Microsoft.AspNet.Identity;
using Sisnet.Models;
using Sisnet.Models.Carrito;
using Sisnet.Models.Clientes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Sisnet.Controllers.Carrito
{
    public class CarritoController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        [AllowAnonymous]
        // GET: Carrito
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = ("Cliente"))]
        public ActionResult ConfirmarPedido()
        {
            var Cliente = db.clientes.Find(User.Identity.GetUserId());
            ViewBag.Folio = Cliente.idPersona + DateTime.UtcNow.ToString();

            return View();
        }

        [HttpPost]
        [Authorize(Roles = ("Cliente"))]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmarPedido(ConfirmarPedidoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var cliente = db.clientes.Find(User.Identity.GetUserId());
                db.Entry(cliente).Collection("HistorialPedidos").Load();
                db.Entry(cliente).Collection("Cards").Load();
                db.Entry(cliente).Collection("DireccionesEnvio").Load();
                if (cliente == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    var card = new Card();
                    card.idTarjeta = Guid.NewGuid().ToString();
                    card.numTarjeta = model.NumTarjeta;
                    card.nombreTarjeta = model.Name;
                    card.codigoSeguridad = model.SecurityCode;
                    card.Cliente = cliente;
                   // cliente.Cards = new List<Card>();
                    cliente.Cards.Add(card);
                    db.Entry(cliente).State = EntityState.Modified;
                   db.SaveChanges();
             

                        var direccion = new DireccionEnvio();
                        direccion.IdDireccion = Guid.NewGuid().ToString();
                        direccion.calle = model.direccionEnvio.calle;
                        direccion.colonia = model.direccionEnvio.colonia;
                        direccion.manzana = model.direccionEnvio.manzana;
                        direccion.lote = model.direccionEnvio.lote;
                        direccion.municipio = model.direccionEnvio.municipio;
                        direccion.estado = model.direccionEnvio.estado;
                        direccion.ciudad = model.direccionEnvio.ciudad;
                        direccion.cp = model.direccionEnvio.cp;
                     //   cliente.DireccionesEnvio = new List<DireccionEnvio>();
                        cliente.DireccionesEnvio.Add(direccion);
                        db.Entry(cliente).State = EntityState.Modified;
                    db.SaveChanges();
                    


                    
                    List<ItemCarrito> itemsCarrito = (List<ItemCarrito>)Session["Carrito"];
                    List<ItemPedido> itemPedidos = new List<ItemPedido>();
                    var Pedido = new Pedido
                    {
                        IdPedido = Guid.NewGuid().ToString()
                       
                    };
                    Pedido.FolioPedido = Pedido.IdPedido + cliente.idPersona;
                    Pedido.Estado = Pedido.StatusPedido.SOLICITADO;
                    db.Pedidos.Add(Pedido);
                    db.SaveChanges();

                    double total = 0;
                    foreach (var item in itemsCarrito)
                    {
                        var ItemPedido = new ItemPedido();
                        ItemPedido.id = Guid.NewGuid().ToString();
                        ItemPedido.idProducto = item.Producto.IdProducto;
                        ItemPedido.Cantidad = item.Cantidad;
                        total = item.Cantidad * item.Producto.PrecioProducto;
                        ItemPedido.idPedido = Pedido.IdPedido;
                        itemPedidos.Add(ItemPedido);
                     
                    }
                    db.ItemsPedido.AddRange(itemPedidos);
                    db.SaveChanges();

                    

                    Pedido.Total = total;
                   
                    //cliente.HistorialPedidos = new List<Pedido>();
                    cliente.HistorialPedidos.Add(Pedido);

                    db.Entry(cliente).State = EntityState.Modified;
                    db.SaveChanges();
                    

                  

                    try
                    {

                        cliente.HistorialPedidos.Add(Pedido);
                        db.Entry(cliente).State = EntityState.Modified;
                        db.SaveChanges();
                        Session["Carrito"] = null;
                        return RedirectToAction("MisPedidos","Clientes");
                    }
                    catch (Exception e)
                    {

                        Console.WriteLine(e.Message);

                    }
                }
            }
            return View(model);
        }

        [HttpPost]
        public JsonResult agregarProducto(String idProducto, int cantidad)
        {

            if (Session["Carrito"] == null)
            {
                List<ItemCarrito> carritoProductos = new List<ItemCarrito>();
                var producto = db.productos.Find(idProducto);
                ItemCarrito item = new ItemCarrito(producto, cantidad);
                carritoProductos.Add(item);
                Session["Carrito"] = carritoProductos;
            }
            else
            {
                List<ItemCarrito> carritoProductos = (List<ItemCarrito>)Session["Carrito"];
                int IndexExistent = getIndex(idProducto);
                if (IndexExistent == -1)
                {
                    var producto = db.productos.Find(idProducto);

                    carritoProductos.Add(new ItemCarrito(producto, cantidad));
                    Session["Carrito"] = carritoProductos;
                }
                else
                {

                    carritoProductos[IndexExistent].Cantidad += cantidad;
                    Session["Carrito"] = carritoProductos;

                }

            }

            return Json(new { Response = true }, JsonRequestBehavior.AllowGet);
        }

        private int getIndex(String idProducto)
        {
            List<ItemCarrito> compras = (List<ItemCarrito>)Session["Carrito"];
            for (int i = 0; i < compras.Count; i++)
            {

                if (compras[i].Producto.IdProducto == idProducto)
                    return i;
            }

            return -1;
        }



    }
}