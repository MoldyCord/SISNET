﻿using Sisnet.Models;
using System;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace Sisnet.Controllers
{
    public class HomeController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize(Roles ="Admin")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

      
        public ActionResult Inicio() {

            return View();
        }

        public ActionResult Error() {

            return View();
        }

        


        [HttpPost]
        public JsonResult SendMail(String toEmail,String EMailBody,String nombre)
        {
            var response="";
            var EmailSubject = "Nuevo contacto a través del sitio";

            var bodyHtml = System.IO.File.ReadAllText(HttpContext.Server.MapPath("~/Views/Templates/EmailTemplate.html"));
            bodyHtml = bodyHtml.Replace("#NOMBRE#", nombre);
            bodyHtml = bodyHtml.Replace("#CORREO#", toEmail);
            bodyHtml = bodyHtml.Replace("#COMENTARIO", EMailBody);
            var EmailCC = "danieergalvez@gmail.com";

            MailMessage mail = new MailMessage
            {
                From = new MailAddress("sisnet.consulting98@gmail.com")
            };

            mail.To.Add(toEmail);
            mail.Subject = EmailSubject;
            mail.Body = bodyHtml;
            mail.CC.Add(EmailCC);
            mail.IsBodyHtml = true;
            

            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                Credentials = new NetworkCredential("danieergalvez@gmail.com", "Eleshnorn16"),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
               
               
            };
            try
            {
                smtp.Send(mail);
                response = "OK";
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
            finally
            {
                smtp.Dispose();
            }

            return Json(new { Response = response   }, JsonRequestBehavior.AllowGet);

        }
    }
}