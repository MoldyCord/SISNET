namespace Sisnet.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class data : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        idPersona = c.String(nullable: false, maxLength: 128),
                        nombre = c.String(),
                        apellidoPaterno = c.String(),
                        apellidoMaterno = c.String(),
                        CURP = c.String(),
                        RFC = c.String(),
                        Nacionalidad = c.String(),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.idPersona)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserRole = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        idTarjeta = c.String(nullable: false, maxLength: 128),
                        nombreTarjeta = c.String(),
                        numTarjeta = c.String(nullable: false),
                        codigoSeguridad = c.String(nullable: false),
                        Cliente_idPersona = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.idTarjeta)
                .ForeignKey("dbo.Clientes", t => t.Cliente_idPersona)
                .Index(t => t.Cliente_idPersona);
            
            CreateTable(
                "dbo.Direcciones",
                c => new
                    {
                        IdDireccion = c.String(nullable: false, maxLength: 128),
                        calle = c.String(maxLength: 100),
                        manzana = c.String(maxLength: 100),
                        lote = c.String(maxLength: 100),
                        noInterior = c.String(maxLength: 100),
                        noExterior = c.String(maxLength: 100),
                        colonia = c.String(maxLength: 100),
                        municipio = c.String(maxLength: 100),
                        cp = c.String(nullable: false, maxLength: 10),
                        ciudad = c.String(maxLength: 100),
                        estado = c.String(maxLength: 100),
                        pais = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.IdDireccion);
            
            CreateTable(
                "dbo.Pedidos",
                c => new
                    {
                        IdPedido = c.String(nullable: false, maxLength: 128),
                        FolioPedido = c.String(),
                        Total = c.Double(nullable: false),
                        Estado = c.Int(nullable: false),
                        Cliente_idPersona = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdPedido)
                .ForeignKey("dbo.Clientes", t => t.Cliente_idPersona)
                .Index(t => t.Cliente_idPersona);
            
            CreateTable(
                "dbo.ItemsPedido",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        idPedido = c.String(maxLength: 128),
                        idProducto = c.String(maxLength: 128),
                        Cantidad = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pedidos", t => t.idPedido)
                .ForeignKey("dbo.Productos", t => t.idProducto)
                .Index(t => t.idPedido)
                .Index(t => t.idProducto);
            
            CreateTable(
                "dbo.Productos",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        NombreProducto = c.String(nullable: false),
                        PrecioProducto = c.Double(nullable: false),
                        SKU = c.String(),
                        Descripcion = c.String(),
                        marca = c.String(),
                        Modelo = c.String(),
                        numSerie = c.String(),
                        Existencia = c.Int(nullable: false),
                        ImagenProducto_FilePathId = c.Int(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.FilePaths", t => t.ImagenProducto_FilePathId)
                .Index(t => t.ImagenProducto_FilePathId);
            
            CreateTable(
                "dbo.FilePaths",
                c => new
                    {
                        FilePathId = c.Int(nullable: false, identity: true),
                        FileName = c.String(maxLength: 255),
                        FileType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilePathId);
            
            CreateTable(
                "dbo.OrdenesServicio",
                c => new
                    {
                        IdOrden = c.String(nullable: false, maxLength: 128),
                        IdCliente = c.String(),
                        Folio = c.String(),
                        FechaRecibo = c.DateTime(nullable: false),
                        FechaEntrega = c.DateTime(nullable: false),
                        Estado = c.Int(nullable: false),
                        Descripcion = c.String(),
                        Costo = c.Double(nullable: false),
                        Observaciones = c.String(),
                        Cliente_idPersona = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.IdOrden)
                .ForeignKey("dbo.Clientes", t => t.Cliente_idPersona)
                .Index(t => t.Cliente_idPersona);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        idPersona = c.String(nullable: false, maxLength: 128),
                        FechaAfiliacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.idPersona)
                .ForeignKey("dbo.Personas", t => t.idPersona)
                .Index(t => t.idPersona);
            
            CreateTable(
                "dbo.DireccionesEnvio",
                c => new
                    {
                        IdDireccion = c.String(nullable: false, maxLength: 128),
                        Cliente_idPersona = c.String(maxLength: 128),
                        isDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IdDireccion)
                .ForeignKey("dbo.Direcciones", t => t.IdDireccion)
                .ForeignKey("dbo.Clientes", t => t.Cliente_idPersona)
                .Index(t => t.IdDireccion)
                .Index(t => t.Cliente_idPersona);
            
            CreateTable(
                "dbo.ProductosGabinete",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Dimensiones = c.String(),
                        Color = c.String(),
                        Puertos = c.String(),
                        Ventiladores = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.ProductosMemorias",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Capacidad = c.String(),
                        Velocidad = c.String(),
                        Clase = c.String(),
                        Latencia = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.ProductosPeriferico",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Color = c.String(),
                        Tipo = c.String(),
                        Compatibilidad = c.String(),
                        Interfaz = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.ProductosDiscos",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Capacidad = c.Int(nullable: false),
                        Velocidad = c.String(),
                        Puerto = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.ProductosMonitor",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Tamanio = c.String(),
                        SalidasVideo = c.String(),
                        RatioDeRefresh = c.String(),
                        Resolution = c.String(),
                        Color = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
            CreateTable(
                "dbo.ProductosProcesador",
                c => new
                    {
                        IdProducto = c.String(nullable: false, maxLength: 128),
                        Nucleos = c.String(),
                        Hilos = c.String(),
                        VelocidadProcesador = c.String(),
                        CacheProcesador = c.String(),
                    })
                .PrimaryKey(t => t.IdProducto)
                .ForeignKey("dbo.Productos", t => t.IdProducto)
                .Index(t => t.IdProducto);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductosProcesador", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosMonitor", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosDiscos", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosPeriferico", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosMemorias", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.ProductosGabinete", "IdProducto", "dbo.Productos");
            DropForeignKey("dbo.DireccionesEnvio", "Cliente_idPersona", "dbo.Clientes");
            DropForeignKey("dbo.DireccionesEnvio", "IdDireccion", "dbo.Direcciones");
            DropForeignKey("dbo.Clientes", "idPersona", "dbo.Personas");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Personas", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrdenesServicio", "Cliente_idPersona", "dbo.Clientes");
            DropForeignKey("dbo.Pedidos", "Cliente_idPersona", "dbo.Clientes");
            DropForeignKey("dbo.ItemsPedido", "idProducto", "dbo.Productos");
            DropForeignKey("dbo.Productos", "ImagenProducto_FilePathId", "dbo.FilePaths");
            DropForeignKey("dbo.ItemsPedido", "idPedido", "dbo.Pedidos");
            DropForeignKey("dbo.Cards", "Cliente_idPersona", "dbo.Clientes");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.ProductosProcesador", new[] { "IdProducto" });
            DropIndex("dbo.ProductosMonitor", new[] { "IdProducto" });
            DropIndex("dbo.ProductosDiscos", new[] { "IdProducto" });
            DropIndex("dbo.ProductosPeriferico", new[] { "IdProducto" });
            DropIndex("dbo.ProductosMemorias", new[] { "IdProducto" });
            DropIndex("dbo.ProductosGabinete", new[] { "IdProducto" });
            DropIndex("dbo.DireccionesEnvio", new[] { "Cliente_idPersona" });
            DropIndex("dbo.DireccionesEnvio", new[] { "IdDireccion" });
            DropIndex("dbo.Clientes", new[] { "idPersona" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.OrdenesServicio", new[] { "Cliente_idPersona" });
            DropIndex("dbo.Productos", new[] { "ImagenProducto_FilePathId" });
            DropIndex("dbo.ItemsPedido", new[] { "idProducto" });
            DropIndex("dbo.ItemsPedido", new[] { "idPedido" });
            DropIndex("dbo.Pedidos", new[] { "Cliente_idPersona" });
            DropIndex("dbo.Cards", new[] { "Cliente_idPersona" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Personas", new[] { "ApplicationUser_Id" });
            DropTable("dbo.ProductosProcesador");
            DropTable("dbo.ProductosMonitor");
            DropTable("dbo.ProductosDiscos");
            DropTable("dbo.ProductosPeriferico");
            DropTable("dbo.ProductosMemorias");
            DropTable("dbo.ProductosGabinete");
            DropTable("dbo.DireccionesEnvio");
            DropTable("dbo.Clientes");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.OrdenesServicio");
            DropTable("dbo.FilePaths");
            DropTable("dbo.Productos");
            DropTable("dbo.ItemsPedido");
            DropTable("dbo.Pedidos");
            DropTable("dbo.Direcciones");
            DropTable("dbo.Cards");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Personas");
        }
    }
}
